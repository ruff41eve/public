<?php
require_once 'lib/twitteroauth.php';
 
define('CONSUMER_KEY', '***');
define('CONSUMER_SECRET', '***');
define('ACCESS_TOKEN', '***');
define('ACCESS_TOKEN_SECRET', '***');
 
function auto_follow()
{
    $toa = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET);
 
    $followers = $toa->get('followers/ids', array('cursor' => -1));
    $friends = $toa->get('friends/ids', array('cursor' => -1));
 
    foreach ($friends->ids as $i => $id) {
        if (empty($followers->ids) or !in_array($id, $followers->ids)) {
            $toa->post('friendships/destroy', array('user_id' => $id));
        }
    }
}
 
auto_follow();