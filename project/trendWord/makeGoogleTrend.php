<?php

function google_trend_rss()
{
	# Googleトレンドをワードを取得します。
    $rssFile = fopen("/***/google.txt","w");
	$search_url = "https://ajax.googleapis.com/ajax/services/feed/load?v=1.0&q=http://www.google.co.jp/trends/hottrends/atom/hourly&num=10";

	// APIから検索結果をjsonで取得
	$json = file_get_contents("$search_url");
	
	// 受け取ったJSONを連想配列に変換
 	$data = json_decode( $json , true );
 	
 	$title = " ".$data[responseData][feed][entries][0][title];
 	
 	$title = str_replace('...', "", $title);
 	$title = str_replace(', ', "\n ", $title);
 	
	fwrite($rssFile, $title);

	fclose($rssFile);
}
 
google_trend_rss();
?>